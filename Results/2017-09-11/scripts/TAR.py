import cortex
import random
import yaml
import os

msg_size = 0
initialized = False
ring = []

def find_config():
  for f in os.listdir('.'):
    if f.endswith('.yml'):
      return f
  return None

def initialize():
  global msg_size, initialized
  filename = find_config()
  if filename == None:
    raise RuntimeError("configuration file not found")
  yaml_file = open(filename)
  yaml_data = yaml_file.read()
  params = yaml.load(yaml_data)
  params = params['codes']['parameters']
  msg_size = params['msg_size']
  initialized = True

 
def AllGather(rank,size,msg_size):
	global ring
	pos = ring.index(rank)
	for i in range(0,size-1):
		# Send all data to next in ring
		dst = pos + 1
		if (dst >= size):
			dst -= size
		# Receive one data from previous in ring
		src = pos -1 
		if (src < 0):
			src += size
		s = cortex.MPI_Status()
		# Due to handshake, need to use MPI_Sendrecv
		cortex.MPI_Sendrecv(rank,sendcount=msg_size,sendtype=cortex.MPI_BYTE,dest=ring[dst],sendtag=1234,recvcount=msg_size,recvtype=cortex.MPI_BYTE,source=ring[src],recvtag=1234,comm=cortex.MPI_COMM_WORLD,status=s)					
						
# Sort nodes according to their compute node id
def CreateRing(world_size):
	global ring
	ranks = {}
	for i in range(0,world_size):
		ranks[i] = cortex.get_compute_node_id_from_rank(i)
	sorted_ranks = sorted(ranks.items(),key=lambda x: x[1])
	ring = [x[0] for x in sorted_ranks]

def GenerateEvents(thread):
	ws = cortex.comm_world_size()
	global initialized, num_iter, msg_size
	if(not initialized):
		initialize()
		CreateRing(ws)
	AllGather(thread,ws,msg_size)

