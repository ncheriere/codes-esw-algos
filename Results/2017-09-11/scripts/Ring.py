import cortex
import random
import yaml
import os

msg_size = 0
initialized = False

def find_config():
  for f in os.listdir('.'):
    if f.endswith('.yml'):
      return f
  return None

def initialize():
  global msg_size, initialized
  filename = find_config()
  if filename == None:
    raise RuntimeError("configuration file not found")
  yaml_file = open(filename)
  yaml_data = yaml_file.read()
  params = yaml.load(yaml_data)
  params = params['codes']['parameters']
  msg_size = params['msg_size']
  initialized = True

 
def AllGather(rank,size,msg_size):
	for i in range(0,size-1):
		# Send all data to next in ring
		dst = rank + 1
		if (dst >= size):
			dst -= size
		# Receive one data from previous in ring
		src = rank -1 
		if (src < 0):
			src += size
		s = cortex.MPI_Status()
		# Due to handshake mechanism, there is a need to use a sendrecv operation
		cortex.MPI_Sendrecv(rank,sendcount=msg_size,sendtype=cortex.MPI_BYTE,dest=dst,sendtag=1234,recvcount=msg_size,recvtype=cortex.MPI_BYTE,source=src,recvtag=1234,comm=cortex.MPI_COMM_WORLD,status=s)					

def GenerateEvents(thread):
	ws = cortex.comm_world_size()
	global initialized, msg_size
	if(not initialized):
		initialize()
	AllGather(thread,ws,msg_size)

