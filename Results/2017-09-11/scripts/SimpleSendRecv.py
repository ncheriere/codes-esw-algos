import cortex
import random
import yaml
import os

num_iter = 0
msg_size = 0
initialized = False

def find_config():
  for f in os.listdir('.'):
    if f.endswith('.yml'):
      return f
  return None

def initialize():
  global num_iter, msg_size, initialized
  filename = find_config()
  if filename == None:
    raise RuntimeError("configuration file not found")
  yaml_file = open(filename)
  yaml_data = yaml_file.read()
  params = yaml.load(yaml_data)
  params = params['codes']['parameters']
  num_iter  = params['bcast_num_iter']
  msg_size = params['bcast_msg_size']
  initialized = True

# 
def Scatter(rank,size,msg_size):
	if (rank == 1):
		# Receive one data from rank 0
		s = cortex.MPI_Status()
		cortex.MPI_Recv(rank,count=msg_size,datatype=cortex.MPI_BYTE,source=0,tag=1234,comm=cortex.MPI_COMM_WORLD,status=s)
	if (rank == 0):
		# Send all data to destinations one by one
		cortex.MPI_Send(rank,count=msg_size,datatype=cortex.MPI_BYTE,dest=1,tag=1234,comm=cortex.MPI_COMM_WORLD)
						


def GenerateEvents(thread):
	ws = cortex.comm_world_size()
	global initialized, num_iter, msg_size
	if(not initialized):
		initialize()
	for i in range(0,num_iter):
		Scatter(thread,ws,msg_size)

