import cortex
import random
import yaml
import os

explored = False

def Explore():
	print ""
	for i in range(0,9):
		for j in range(0,4):
			rtr = i * 96 + j * 4
			neighbors = cortex.get_router_neighbors(rtr)
			print rtr,":\t",neighbors
			print rtr," -\t",[int(x/96) for x in neighbors]
			print rtr," -\t",[x%96 for x in neighbors]
			print ""
	bw = cortex.get_router_link_bandwidth(0,480)
	print bw
	print "Bw 0 - 1: ",cortex.get_router_link_bandwidth(0,1)
	print "Bw 0 - 16: ",cortex.get_router_link_bandwidth(0,16)
	
def PrintTopologyAt(rank):
  print "## Rank ", rank, " is mapped on compute node ", cortex.get_compute_node_id_from_rank(rank)
  print "## Full allocation : ", cortex.get_full_allocation()
  print "## Number of compute nodes: ", cortex.get_number_of_compute_nodes()
  print "## Number of routers: ", cortex.get_number_of_routers()
  bw = cortex.get_compute_node_bandwidth(0)
  print "## Node 0 has bandwidth ",bw," to its router"
  neighbors = cortex.get_router_neighbors(0)
  print "## Neighbors of router 0 are ", neighbors
  bw = cortex.get_router_link_bandwidth(0,neighbors[0])
  print "## Bandwidth between router 0 and router ", neighbors[0]," is ", bw
  loc = cortex.get_compute_node_location(0)
  print "## Compute node 0 is at location ", loc
  loc = cortex.get_router_location(0)
  print "## Router 0 is at location ", loc
  r = cortex.get_router_from_compute_node(0)
  print "## Compute node 0 is connected to router ", r
  cn = cortex.get_router_compute_nodes(0)
  print "## Router 0 is connected to compute nodes ", cn

def Exchange(rank):
	msg_size = 128
	nb_msg = 1024*128
	if (rank == 1):
		for it in range(0,nb_msg):
			s = cortex.MPI_Status()
			cortex.MPI_Recv(rank,count=msg_size,datatype=cortex.MPI_BYTE,source=0,tag=1234,comm=cortex.MPI_COMM_WORLD,status=s)
	if (rank == 0):
		for it in range(0,nb_msg):
			cortex.MPI_Isend(rank, count=msg_size, datatype=cortex.MPI_BYTE, dest=1, tag=1234, comm=cortex.MPI_COMM_WORLD, request=it)
		for it in range(0,nb_msg):
			s = cortex.MPI_Status()
			cortex.MPI_Wait(rank, request=it, status=s)	


def GenerateEvents(thread):
	global explored
	if (not explored):
#		Explore()
		explored = True
	Exchange(thread)	
