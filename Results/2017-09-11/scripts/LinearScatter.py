import cortex
import random
import yaml
import os

msg_size = 0
initialized = False

def find_config():
  for f in os.listdir('.'):
    if f.endswith('.yml'):
      return f
  return None

def initialize():
  global msg_size, initialized
  filename = find_config()
  if filename == None:
    raise RuntimeError("configuration file not found")
  yaml_file = open(filename)
  yaml_data = yaml_file.read()
  params = yaml.load(yaml_data)
  params = params['codes']['parameters']
  msg_size = params['msg_size']
  initialized = True

# Linear scatter 
# rank - MPI rank of the node
# size - Size of the allocation, number of nodes participating in the communication
# msg_size - size of each message to transmit 
def Scatter(rank,size,msg_size):
	# data sent and received 
	balance = 0
	if (rank == 0):
		balance += msg_size*size
	# If the node isn't the root, wait to receive data
	if (rank != 0):
		# Receive one data from rank 0
		s = cortex.MPI_Status()
		cortex.MPI_Recv(rank,count=msg_size,datatype=cortex.MPI_BYTE,source=0,tag=1234,comm=cortex.MPI_COMM_WORLD,status=s)
		balance += msg_size
	# If the node is the root send to all nodes 
	if (rank == 0):
		# Send all data to destinations one by one
		for dst in range(1,size):
			cortex.MPI_Isend(rank, count=msg_size, datatype=cortex.MPI_BYTE, dest=dst, tag=1234, comm=cortex.MPI_COMM_WORLD, request=dst)
			balance -= msg_size
		# Wait for all transfers to finish
		for dst in range(1,size):
			s = cortex.MPI_Status()
			cortex.MPI_Wait(rank, request=dst, status=s)
	assert balance == msg_size


def GenerateEvents(thread):
	ws = cortex.comm_world_size()
	global initialized, msg_size
	if(not initialized):
		initialize()
	Scatter(thread,ws,msg_size)

