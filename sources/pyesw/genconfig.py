import re

# --------------------------------------------------------------------------- #
#	gen_dict_from_template
# Takes the file name of a template, and builds a dictionary of its parameters
# with their default values.
# --------------------------------------------------------------------------- #
def gen_dict_from_template(template):
  p = re.compile('<<(\w+):([^>]+)>>|<<(\w+)>>')
  f = open(template)
  var = dict()
  for line in f:
    m = p.search(line)
    if m == None: continue
    if m.group(1) != None and m.group(2) != None:
      v = m.group(1)
      x = m.group(2)
      var[v] = x
  f.close()
  return var

# --------------------------------------------------------------------------- #
# 	gen_conf_from_template
# Takes the file name of a template, a dictionary of variables, and the output
# file name, and fills in the template with the variables defined in the
# dictionary. Raises an exception if a mandatory variable is not defined.
# --------------------------------------------------------------------------- #
def gen_conf_from_template(template, variables, outfile):
  p = re.compile('<<(\w+):([^>]+)>>|<<(\w+)>>')
  f = open(template)
  out = open(outfile,'w+')
  for line in f:
    m = p.search(line)
    if m == None:
      out.write(line)
      continue
    # the line matches the regex, figure out the name of the parameter
    v = ''
    x = None
    if m.group(3) != None:
      v = m.group(3)
    else:
      v = m.group(1)
    # check if the parameter is defined in the dictionary
    if not (v in variables):
        raise RuntimeError("'"+v+"' not defined in parameters dictionary")
    # replace the parameter by its value in the configuration
    line = re.sub('<<[^>]*>>', str(variables[v]), line)
    out.write(line)
  f.close()
  out.close()

def merge_input_parameters(params, var):
  # var is from the template, may include expressions
  # params is from the user
  for k in params:
      var[k] = params[k]
  for k in var:
    if not (k in params):
      var[k] = eval(str(var[k]),dict(),var)
  for k in var:
    params[k] = var[k]
