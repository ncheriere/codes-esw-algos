import io;
import python;
import string;
import sys;
import esw.util;

(int num) count_instances(string filename) {
  pycmd = sprintf("str(CodesInstance.count_instances('%s'))", filename);
  s = python("from pyesw.codes import *", pycmd);
  num = string2int(s);
}

run_instance(string config, int i) {
  a = [ int2string(i), config ];
  s = sprintf("str(CodesInstance.get_tasks('%s',%d))", config, i);
  p_str = python("from pyesw.codes import *", s);
  p = string2int(p_str);
  @par=p launch_turbine("subworkflow.tic", a);
}

@suppress=unused_output
app (file x) clean_partial_run(string dir) {
  "rm" "-rf" dir
}

config = argp(1);

// Step 1: Count the number of configurations
int n = count_instances(config);

// Step 2: Go through all configurations
foreach i in [0:(n-1):1] {
  dir = sprintf("%012d",i);
  if(!file_exists(dir)) {
    run_instance(config,i);
  } else {
    done = sprintf("%s/completed",dir);
    if(!file_exists(done)) {
			clean_partial_run(dir) =>
			printf("Restarting instance %s",dir) =>
			run_instance(config,i);
		}
  }
}
