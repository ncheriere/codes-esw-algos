import io;
import python;
import string;
import sys;
import esw.util;

/* Step 2.1: prepare the directory and create input files */
(string cmd_args) prepare_run(string filename, int i) {
  pycmd = sprintf(
----
CodesInstance.get_instance('%s',%d).prepare().get_codes_cmd()
----,
  filename,i);
  cmd_args = python("from pyesw.codes import *",pycmd);
}

/* Step 2.2: run CODES */
(int status) run_codes(int p, string arguments[]) {
  status = @par=p launch("./run-codes-par.sh", arguments);
  printf("Instance %s exited with status %d", arguments[0], status);
}

app post_process(string instance) {
  "./post-process.sh" instance
}

i = string2int(argp(1));
yaml_file = argp(2);

cmd_args_str = prepare_run(yaml_file, i);
cmd_args = split(cmd_args_str," ");
p = turbine_workers();
status = run_codes(p,cmd_args);
wait(status) {
  post_process(cmd_args[0]);
}
