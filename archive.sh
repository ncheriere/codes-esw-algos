#!/bin/bash

echo "Archiving "$1

cd $1

echo "Deleting useless files"

find . -name "completed" -delete
find . -name "alloc.conf" -delete
find . -name "samplir-dir" -delete
find . -name "dragonfly-msg-stat.meta" -delete
find . -name "*.py" -delete

cp -r ../scripts/ ./

cd ..

echo "Archiving"

tar czf $1.tar.gz $1
