import cortex
import random
import yaml
import os

msg_size = 0
initialized = False
grps_topo = ([],[])
rtrs_topo = {}
nds_topo = {}

def find_config():
	for f in os.listdir('.'):
		if f.endswith('.yml'):
			return f
	return None

def initialize():
	global msg_size, initialized
	filename = find_config()
	if filename == None:
		raise RuntimeError("configuration file not found")
	yaml_file = open(filename)
	yaml_data = yaml_file.read()
	params = yaml.load(yaml_data)
	params = params['codes']['parameters']
	msg_size = params['msg_size']
	initialized = True

# Sends data 
# param current rank, array of dests, array of msg_sizes
def BinomialScatter(sender_rank,dests,msg_size,ws,step):
	size = len(dests)
	assert size == len(msg_size)
	if (1 != dests.count(sender_rank)):
		print dests.count(sender_rank)
		print dests
		assert 1 == dests.count(sender_rank)
	rank = dests.index(sender_rank)
	mask = 0x1
	balance = 0
	# receive phase
	while(mask < size):
		if(rank & mask):
			src = rank - mask
			assert src >= 0
			s = cortex.MPI_Status()
			data_size = 0
			for i in range(0,mask):
				if (rank+i>=size):
					break
				data_size += msg_size[rank+i]
			balance += data_size
			cortex.MPI_Recv(rank,count=data_size,datatype=cortex.MPI_BYTE,source=dests[src],tag=1234,comm=cortex.MPI_COMM_WORLD,status=s)
			break
		mask <<= 1
	# send phase
	mask >>= 1;
	mask2 = mask
	while(mask > 0):
		if(rank + mask < size):
			dst = rank + mask
			assert dst < size
			data_size = 0
			for i in range(0,mask):
				if (dst+i>=size):
					break
				data_size += msg_size[dst+i]
			balance -= data_size
			cortex.MPI_Isend(rank,count=data_size,datatype=cortex.MPI_BYTE,dest=dests[dst],tag=1234,comm=cortex.MPI_COMM_WORLD, request=step*ws*ws+rank*ws+dests[dst])
		mask >>= 1
	while (mask2 > 0):
		if (rank + mask2 < size):
			dst = rank + mask2
			assert dst < size
			s = cortex.MPI_Status()
			cortex.MPI_Wait(rank,request=step*ws*ws+rank*ws+dests[dst],status=s)
		mask2 >>= 1
	return balance

# GreadyBalance sort nodes in a naive way in order to 
# balance the amount of data sent during the scatter operation
# nodes_in - nodes id
# data_in - data to send to the nodes 
# mask - corresponding step in the scatter operation 
# return (nodes, data) 
def GreadyBalance(nodes_in,data_in,mask):
	total = sum(data_in)
	nb = len(nodes_in)
	assert nb == len(data_in)
	if (mask == 0):
		return (nodes_in,data_in)
	if (nb <= mask):
		return GreadyBalance(nodes_in,data_in,mask>>1)
	# create two groups greedily balanced
	# sender can not change, and must be in the head of the first groups of nodes
	nodes_g1 = [nodes_in[0]]
	nodes_g2 = []
	data_g1 = [data_in[0]]
	data_g2 = []
	size_g1 = mask -1
	size_g2 = nb - mask
	total_g1 = data_in[0]
	total_g2 = 0
	del data_in[0]
	del nodes_in[0]
	while (len(nodes_in) != 0):
		current_weight = data_in[0]
		total -= current_weight
		per_missing_node = total/(size_g1 + size_g2)
		if ((size_g1 > 0 and per_missing_node * size_g1 + total_g1 <= per_missing_node * size_g2 + total_g2) or size_g2 == 0):
			# Add data and node to g1
			nodes_g1.append(nodes_in[0])
			data_g1.append(data_in[0])
			size_g1 -= 1
			total_g1 += current_weight
		else:
			# put node in groupe 2
			nodes_g2.append(nodes_in[0])
			data_g2.append(data_in[0])
			size_g2 -= 1
			total_g2 += current_weight
		del nodes_in[0]
		del data_in[0]
	# recursive op on groups
	(bn1,bd1) = GreadyBalance(nodes_g1,data_g1,mask>>1) 
	(bn2,bd2) = GreadyBalance(nodes_g2,data_g2,mask>>1) 
	# append outputs bigger amount of data first
	# return tuple of values
	return (bn1+bn2,bd1+bd2)

# GLF scatter data first among groups, then between routers, lastly 
# between nodes attached to each router
def GLFScatter(rank,world_size,msg_size):
	global grps_topo, rtrs_topo, nds_topo
	# grps_topo : (rep,data)
	# rtrs_topo : dictionnary #grp : (reps,data)
	# nds_topo : dictionnary (#grps,#rtrs) : [nodes]
	balance = 0
	# Sort data according to groups
	# List ranks in the same group
	# List ranks attached to the same router
	grps = {} # dictionnary #grp : [rep,data]
	rtrs = {} # dictionnary (#grp,#rtr) : [rep,data]
	# Get position of current rank
	pos = cortex.get_compute_node_location(cortex.get_compute_node_id_from_rank(rank))
	if (len(nds_topo) == 0):
		# Retreive important data
		for rk in range(0,world_size):
			rk_id = cortex.get_compute_node_id_from_rank(rk)
			rk_pos = cortex.get_compute_node_location(rk_id)
			# put as grps
			if (grps.has_key(rk_pos[0])):
				grps[rk_pos[0]][1] += msg_size
			else:
				grps[rk_pos[0]] = [rk,msg_size]
				rtrs[rk_pos[0]] = {}
			# store as router
			if (rtrs[rk_pos[0]].has_key(rk_pos[1])):
				rtrs[rk_pos[0]][rk_pos[1]][1] += msg_size
			else:
				rtrs[rk_pos[0]][rk_pos[1]] = [rk,msg_size]
			# Store all nodes
			if (nds_topo.has_key((rk_pos[0],rk_pos[1]))):
				nds_topo[(rk_pos[0],rk_pos[1])] = nds_topo[(rk_pos[0],rk_pos[1])] + [rk]
			else:
				nds_topo[(rk_pos[0],rk_pos[1])] = [rk]
						
		# Sort data according to data to transfer 
		grps_all_data = sorted(grps.items(), key=lambda x: x[1][1])
		grps_all_data.reverse()
		grps_rep = [x[1][0] for x in grps_all_data]
		# put back root at the beginning
		index = grps_rep.index(0)
		del grps_rep[index]
		grps_rep = [0] + grps_rep
		grps_data = [x[1][1] for x in grps_all_data]
		data_root = grps_data[index]
		del grps_data[index]
		grps_data = [data_root] + grps_data
		grps_len = len(grps_all_data)
		grps_mask = 0x1
		while (grps_mask<<1 < grps_len):
			grps_mask<<=1
		grps_topo = GreadyBalance(grps_rep,grps_data,grps_mask)
		# Process routers data and representatives
		grps_keys = grps.keys()
		for k in grps_keys:
			# Sort data according to routers and pick representatives
			rtrs_all_data = sorted(rtrs[k].items(), key=lambda x: x[1][1])
			rtrs_all_data.reverse()
			rtrs_rep = [x[1][0] for x in rtrs_all_data]
			# put back root at the beginning
			index = rtrs_rep.index(min(rtrs_rep))
			root = rtrs_rep[index]
			del rtrs_rep[index]
			rtrs_rep = [root] + rtrs_rep
			rtrs_data = [x[1][1] for x in rtrs_all_data]
			data_root = rtrs_data[index]
			del rtrs_data[index]
			rtrs_data = [data_root] + rtrs_data
			rtrs_len = len(rtrs_all_data)
			rtrs_mask = 0x1
			while (rtrs_mask<<1 < rtrs_len):
				rtrs_mask<<=1
			rtrs_topo[k] = GreadyBalance(rtrs_rep,rtrs_data,rtrs_mask)
	#
	grp_rep = grps_topo[0]	
	if (grp_rep.count(rank) > 0):
		grp_dat = grps_topo[1] 
		BinomialScatter(rank,grp_rep,grp_dat, world_size, 0)
	# Binomial Scatter among router representatives
	rtr = rtrs_topo[pos[0]]
	if (rtr[0].count(rank) > 0):
		BinomialScatter(rank,rtr[0],rtr[1], world_size, 1)
	nds = nds_topo[(pos[0],pos[1])]
	# Binomial scatter within router
	msg_sizes = [msg_size]*len(nds)	
	BinomialScatter(rank,nds,msg_sizes, world_size, 2)	
#	assert balance == msg_size


def GenerateEvents(thread):
	ws = cortex.comm_world_size()
	global initialized, msg_size
	if(not initialized):
		initialize()
	GLFScatter(thread,ws,msg_size)

