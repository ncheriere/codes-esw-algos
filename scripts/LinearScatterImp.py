import cortex
import random
import yaml
import os

msg_size = 0
initialized = False

def find_config():
  for f in os.listdir('.'):
    if f.endswith('.yml'):
      return f
  return None

def initialize():
  global msg_size, initialized
  filename = find_config()
  if filename == None:
    raise RuntimeError("configuration file not found")
  yaml_file = open(filename)
  yaml_data = yaml_file.read()
  params = yaml.load(yaml_data)
  params = params['codes']['parameters']
  msg_size = params['msg_size']
  initialized = True

target_size = 256

# Linear scatter 
# rank - MPI rank of the node
# size - Size of the allocation, number of nodes participating in the communication
# msg_size - size of each message to transmit 
def Scatter(rank,size,msg_size):
	global target_size
	step = int(target_size/msg_size) + 1
	print ""
	# data sent and received 
	balance = 0
	if (rank == 0):
		balance += msg_size*size
	# If the node isn't the root, wait to receive data
	print "First part"
	if (rank != 0 and rank % step == 0):
		# Receive one data from rank 0
		data = min(step,size-rank)*msg_size
		s = cortex.MPI_Status()
		cortex.MPI_Recv(rank,count=data,datatype=cortex.MPI_BYTE,source=0,tag=1234,comm=cortex.MPI_COMM_WORLD,status=s)
		balance += data
		print rank," receives ",data," B from 0"
	# If the node is the root send to all nodes 
	if (rank == 0):
		# Send all data to destinations one by one
		for dst in range(1,int((size-1)/step)+1):
			data = min(step,size-dst*step)*msg_size
			cortex.MPI_Isend(rank, count=data, datatype=cortex.MPI_BYTE, dest=dst*step, tag=1234, comm=cortex.MPI_COMM_WORLD, request=dst)
			balance -= data
			print rank," sends ",data," B to ",dst*step
		# Wait for all transfers to finish
		for dst in range(1,int((size-1)/step)+1):
			s = cortex.MPI_Status()
			cortex.MPI_Wait(rank, request=dst, status=s)
	if (step != 1):
		print "Second part"
		if (rank % step != 0):
			src = int(rank/step)*step
			# Receive one data from rank 0
			s = cortex.MPI_Status()
			cortex.MPI_Recv(rank,count=msg_size,datatype=cortex.MPI_BYTE,source=src,tag=1234,comm=cortex.MPI_COMM_WORLD,status=s)
			balance += msg_size
			print rank," receives ",msg_size," B from ",src
		# If the node is the root send to all nodes 
		if (rank % step == 0):
			# Send all data to destinations one by one
			for dst in range(1,min(step,size-rank)):
				cortex.MPI_Isend(rank, count=msg_size, datatype=cortex.MPI_BYTE, dest=rank+dst, tag=1234, comm=cortex.MPI_COMM_WORLD, request=size+rank+dst)
				balance -= msg_size
				print rank," sends ",msg_size," B to ",rank+dst
			# Wait for all transfers to finish
			for dst in range(1,min(step,size-rank)):
				s = cortex.MPI_Status()
				cortex.MPI_Wait(rank, request=size+rank+dst, status=s)	
	print "rank: ",rank," - balance: ",balance
	assert balance == msg_size


def GenerateEvents(thread):
	ws = cortex.comm_world_size()
	global initialized, msg_size
	if(not initialized):
		initialize()
	Scatter(thread,ws,msg_size)

