import cortex
import random
import yaml
import os

msg_size = 0
initialized = False

def find_config():
  for f in os.listdir('.'):
    if f.endswith('.yml'):
      return f
  return None

def initialize():
  global msg_size, initialized
  filename = find_config()
  if filename == None:
    raise RuntimeError("configuration file not found")
  yaml_file = open(filename)
  yaml_data = yaml_file.read()
  params = yaml.load(yaml_data)
  params = params['codes']['parameters']
  msg_size = params['msg_size']
  initialized = True

 
def AllGather(rank,size,msg_size):
	print ""
	mask = 0x1
	balance = msg_size
	while ((mask << 1) < size and mask < 16):
		s = cortex.MPI_Status()
		# Send all data to next in ring
		dst = rank + mask
		if (dst >= size):
			dst -= size
		# Receive one data from previous in ring
		src = rank - mask
		if (src < 0):
			src += size
		cortex.MPI_Sendrecv(rank,sendcount=msg_size*mask,sendtype=cortex.MPI_BYTE,dest=dst,sendtag=1234,recvcount=msg_size*mask,recvtype=cortex.MPI_BYTE,source=src,recvtag=1234,comm=cortex.MPI_COMM_WORLD,status=s)					
		balance += msg_size*mask
#		print "Sending ",msg_size*mask,"b to ",dst 
#		print "Receiving ",msg_size*mask,"b from ",src
		mask <<= 1
	# last step
	data = ( size - mask ) * msg_size
	dst = rank + mask
	if (dst >= size):
		dst -= size
	# Receive one data from previous in ring
	src = rank - mask
	if (src < 0):
		src += size
	s = cortex.MPI_Status()
	cortex.MPI_Sendrecv(rank,sendcount=data,sendtype=cortex.MPI_BYTE,dest=dst,sendtag=1234,recvcount=data,recvtype=cortex.MPI_BYTE,source=src,recvtag=1234,comm=cortex.MPI_COMM_WORLD,status=s)					
#	print "Sending ",data,"b to ",dst 
#	print "Receiving ",data,"b from ",src
	balance += data
	assert balance == msg_size * size

def GenerateEvents(thread):
	ws = cortex.comm_world_size()
	global initialized, msg_size
	if(not initialized):
		initialize()
	AllGather(thread,ws,msg_size)

