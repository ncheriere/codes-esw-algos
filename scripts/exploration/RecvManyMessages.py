import cortex
import random
import yaml
import os

msg_size = 0
initialized = False

def find_config():
  for f in os.listdir('.'):
    if f.endswith('.yml'):
      return f
  return None

def initialize():
  global msg_size, initialized
  filename = find_config()
  if filename == None:
    raise RuntimeError("configuration file not found")
  yaml_file = open(filename)
  yaml_data = yaml_file.read()
  params = yaml.load(yaml_data)
  params = params['codes']['parameters']
  msg_size = params['msg_size']
  initialized = True

def Send(rank,world_size,msg_size):
  if (rank == 0):
    for j in range(1, world_size) :
      cortex.MPI_Irecv(rank, count=msg_size, datatype=cortex.MPI_BYTE, source=j, tag=1234, comm=cortex.MPI_COMM_WORLD, request=j)
    for j in range(1,world_size) :
      s = cortex.MPI_Status()
      cortex.MPI_Wait(rank,request=j,status=s)
  if (rank != 0):
    cortex.MPI_Send(rank, count=msg_size, datatype=cortex.MPI_BYTE, dest=0, tag=1234, comm=cortex.MPI_COMM_WORLD)

def GenerateEvents(thread):
  ws = cortex.comm_world_size()
  global initialized, msg_size
  if(not initialized):
    initialize()
  Send(thread,ws,msg_size)

