import cortex
import random
import yaml
import os

msg_size = 0
initialized = False

def find_config():
  for f in os.listdir('.'):
    if f.endswith('.yml'):
      return f
  return None

def initialize():
  global msg_size, initialized
  filename = find_config()
  if filename == None:
    raise RuntimeError("configuration file not found")
  yaml_file = open(filename)
  yaml_data = yaml_file.read()
  params = yaml.load(yaml_data)
  params = params['codes']['parameters']
  msg_size = params['msg_size']
  initialized = True

def SendOne(rank,msg_size):
  if (rank == 0):
    s = cortex.MPI_Status()
    cortex.MPI_Sendrecv(rank, sendcount=msg_size, sendtype=cortex.MPI_BYTE, dest=1, sendtag=1234, recvcount=msg_size, recvtype=cortex.MPI_BYTE, source=1, recvtag=1234, comm=cortex.MPI_COMM_WORLD, status=s)
  if (rank == 1):
    s = cortex.MPI_Status()
    cortex.MPI_Sendrecv(rank, sendcount=msg_size, sendtype=cortex.MPI_BYTE, dest=0, sendtag=1234, recvcount=msg_size, recvtype=cortex.MPI_BYTE, source=0, recvtag=1234, comm=cortex.MPI_COMM_WORLD, status=s)

def GenerateEvents(thread):
  global initialized, num_iter, msg_size
  if(not initialized):
    initialize()
  SendOne(thread,msg_size)

