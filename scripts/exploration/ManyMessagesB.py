import cortex
import random
import yaml
import os

nb_msg = 0
msg_size = 0
initialized = False

def find_config():
  for f in os.listdir('.'):
    if f.endswith('.yml'):
      return f
  return None

def initialize():
  global msg_size, initialized, nb_msg
  filename = find_config()
  if filename == None:
    raise RuntimeError("configuration file not found")
  yaml_file = open(filename)
  yaml_data = yaml_file.read()
  params = yaml.load(yaml_data)
  params = params['codes']['parameters']
  msg_size = params['msg_size']
  nb_msg = params['nb_msg']
  initialized = True

def Send(rank,world_size,msg_size,nb_msg):
  if (rank == 0):
    for j in range(1, world_size) :
      for i in range(0, nb_msg) :
        cortex.MPI_Send(rank, count=msg_size, datatype=cortex.MPI_BYTE, dest=j, tag=1234, comm=cortex.MPI_COMM_WORLD)
  if (rank != 0):
    for i in range(0,nb_msg) :
      s = cortex.MPI_Status()
      cortex.MPI_Recv(rank,count=msg_size,datatype=cortex.MPI_BYTE,source=0,tag=1234,comm=cortex.MPI_COMM_WORLD,status=s)

def GenerateEvents(thread):
  ws = cortex.comm_world_size()
  global initialized, nb_msg, msg_size
  if(not initialized):
    initialize()
  Send(thread,ws,msg_size,nb_msg)

