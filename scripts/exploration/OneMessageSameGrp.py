import cortex
import random
import yaml
import os

root = 0

# node attached to router in root group that is connected 
# to router attached to destination
final_dest = -1

msg_size = 0

initialized = False

def find_config():
  for f in os.listdir('.'):
    if f.endswith('.yml'):
      return f
  return None

def initialize():
  global msg_size, initialized
  filename = find_config()
  if filename == None:
    raise RuntimeError("configuration file not found")
  yaml_file = open(filename)
  yaml_data = yaml_file.read()
  params = yaml.load(yaml_data)
  params = params['codes']['parameters']
  msg_size = params['msg_size']
  initialized = True

def Explore(world_size):
	print ""
	global final_dest
	root_node_id = cortex.get_compute_node_id_from_rank(0)	
	root_grp = cortex.get_compute_node_location(root_node_id)[0]
	# Test all non root nodes
	for i in range(1,world_size):
		node_id = cortex.get_compute_node_id_from_rank(i)
		# If they are in the root group, they are a candidate
		if (cortex.get_compute_node_location(node_id)[0] == root_grp):
			final_dest = i
			break
	assert(final_dest != -1)
	
def Exchange(rank):
	global msg_size, final_dest
	if (rank == final_dest):
		s = cortex.MPI_Status()
		cortex.MPI_Recv(rank,count=msg_size,datatype=cortex.MPI_BYTE,source=0,tag=1234,comm=cortex.MPI_COMM_WORLD,status=s)
	if (rank == 0):
		cortex.MPI_Send(rank, count=msg_size, datatype=cortex.MPI_BYTE, dest=final_dest, tag=1234, comm=cortex.MPI_COMM_WORLD )

def GenerateEvents(thread):
	global initialized
	ws = cortex.comm_world_size()
	if (not initialized):
		initialize()
		Explore(ws)
		initialized = True
	Exchange(thread)	
