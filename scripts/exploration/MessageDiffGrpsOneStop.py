import cortex
import random
import yaml
import os

root = 0

# node attached to router with link to router in group 0
# i.e. router nb % 16 < 4
final_dest = -1

# node attached to router in root group that is connected 
# to router attached to destination
intermediate = -1

msg_size = 0

initialized = False

def find_config():
  for f in os.listdir('.'):
    if f.endswith('.yml'):
      return f
  return None

def initialize():
  global msg_size, initialized
  filename = find_config()
  if filename == None:
    raise RuntimeError("configuration file not found")
  yaml_file = open(filename)
  yaml_data = yaml_file.read()
  params = yaml.load(yaml_data)
  params = params['codes']['parameters']
  msg_size = params['msg_size']
  initialized = True

# Set the ranks final_dest, and intermediate 
# such as final dest is in another group than root
# intermediate is in the same group as root
# intermediate is two hops away from final_dest
# final_dest is connected to a router that is connected 
# to a router in the root group
def Explore(world_size):
	print ""
	global final_dest, intermediate
	router_intermediate = -1
	root_node_id = cortex.get_compute_node_id_from_rank(0)	
	root_grp = cortex.get_compute_node_location(root_node_id)[0]
	# Test all non root nodes
	for i in range(1,world_size):
		node_id = cortex.get_compute_node_id_from_rank(i)
		# If they are not in the root group, they are a candidate
		if (cortex.get_compute_node_location(node_id)[0] != root_grp):
			router_final = cortex.get_router_from_compute_node(node_id)
			assert(cortex.get_router_location(router_final)[0] == 
					cortex.get_compute_node_location(node_id)[0])
			node_router_neighbors = cortex.get_router_neighbors(router_final)
			# neighbors 0 to 19 are within the same group, so there is only a need to test
			# routers 20 and 21
			router_other_grp1 = cortex.get_router_location(node_router_neighbors[20])[0]
			if ( router_other_grp1 == root_grp ):
				# if the neighbor 20 is in the root group, we have a node that is 
				# at the edge of another group than the root group 
				final_dest = i
				# we save the number of that router, the intermediate router
				router_intermediate = node_router_neighbors[20]
				break	
			router_other_grp2 = cortex.get_router_location(node_router_neighbors[21])[0]
			if ( router_other_grp2 == root_grp ):
				final_dest = i
				router_intermediate = node_router_neighbors[21]
				break	
	assert( final_dest > -1)
	print "Intermediate router: ",router_intermediate
	# Now we look for a node connected to the intermediate router
	for i in range(1,world_size):
		node_id = cortex.get_compute_node_id_from_rank(i)
		if (cortex.get_compute_node_location(node_id)[0] == root_grp):
			node_router = cortex.get_router_from_compute_node(node_id)
			if (node_router == router_intermediate):
				intermediate = i
				break
	print "Destination: ",final_dest
	print "Intermediate: ",intermediate
	assert(intermediate != -1)
	
def Exchange(rank):
	global msg_size, intermediate, final_dest
	if (rank == final_dest):
		s = cortex.MPI_Status()
		cortex.MPI_Recv(rank,count=msg_size,datatype=cortex.MPI_BYTE,source=intermediate,tag=1234,comm=cortex.MPI_COMM_WORLD,status=s)
#	if (rank == 0):
#		cortex.MPI_Send(rank, count=msg_size, datatype=cortex.MPI_BYTE, dest=intermediate, tag=1234, comm=cortex.MPI_COMM_WORLD )
	if (rank == intermediate):
#		s = cortex.MPI_Status()
#		cortex.MPI_Recv(rank,count=msg_size,datatype=cortex.MPI_BYTE,source=0,tag=1234,comm=cortex.MPI_COMM_WORLD,status=s)
		cortex.MPI_Send(rank, count=msg_size, datatype=cortex.MPI_BYTE, dest=final_dest, tag=1234, comm=cortex.MPI_COMM_WORLD )

def GenerateEvents(thread):
	global initialized
	ws = cortex.comm_world_size()
	if (not initialized):
		initialize()
		Explore(ws)
		initialized = True
	Exchange(thread)	
