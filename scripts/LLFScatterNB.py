import cortex
import random
import yaml
import os

msg_size = 0
initialized = False
rtr_to_grp = {}
first_step = ([],[])
second_step = ([],[],[])
rtrs_topo = {}
nds_topo = {}

def find_config():
	for f in os.listdir('.'):
		if f.endswith('.yml'):
			return f
	return None

def initialize():
	global msg_size, initialized
	filename = find_config()
	if filename == None:
		raise RuntimeError("configuration file not found")
	yaml_file = open(filename)
	yaml_data = yaml_file.read()
	params = yaml.load(yaml_data)
	params = params['codes']['parameters']
	msg_size = params['msg_size']
	initialized = True

# Sends data 
# param current rank, array of dests, array of msg_sizes
def BinomialScatter(sender_rank,dests,msg_size,ws,step):
	size = len(dests)
	assert size == len(msg_size)
	if (1 != dests.count(sender_rank)):
		print dests.count(sender_rank)
		print dests
		assert 1 == dests.count(sender_rank)
	rank = dests.index(sender_rank)
	mask = 0x1
	balance = 0
	# receive phase
	while(mask < size):
		if(rank & mask):
			src = rank - mask
			if (src < 0): 
				src += size
			s = cortex.MPI_Status()
			data_size = 0
			for i in range(0,mask):
				if (rank+i>=size):
					break
				data_size += msg_size[rank+i]
			balance += data_size
			print sender_rank," recv ",data_size,"b from " ,dests[src]
			cortex.MPI_Recv(rank,count=data_size,datatype=cortex.MPI_BYTE,source=dests[src],tag=1234,comm=cortex.MPI_COMM_WORLD,status=s)
			break
		mask <<= 1
	# send phase
	mask >>= 1
	mask2 = mask
	while(mask > 0):
		if(rank + mask < size):
			dst = rank + mask
			if(dst >= size): 
				dst -= size
			if (dst == -1):
				print "From ",rank," to ", dst
			data_size = 0
			for i in range(0,mask):
				if (dst+i>=size):
					break
				data_size += msg_size[dst+i]
			balance -= data_size
			print sender_rank," send ",data_size,"b to " ,dests[dst]
			cortex.MPI_Isend(rank, count=data_size, datatype=cortex.MPI_BYTE, dest=dests[dst], tag=1234, comm=cortex.MPI_COMM_WORLD, request=step*ws*ws+rank*ws+dests[dst])
		mask >>= 1;
	while (mask2 > 0):
		if (rank + mask2 < size):
			dst = rank + mask2
			assert dst < size
			s = cortex.MPI_Status()
			cortex.MPI_Wait(rank, request=step*ws*ws+rank*ws+dests[dst], status=s)
		mask2 >>= 1
	return balance

# (nodes, data) sorted by data size, mask
def GreadyBalance(nodes_in,data_in,mask):
	total = sum(data_in)
	nb = len(nodes_in)
	assert nb == len(data_in)
	if (mask == 0):
		return (nodes_in,data_in)
	if (nb <= mask):
		return GreadyBalance(nodes_in,data_in,mask>>1)
	# create two groups greedily balanced
	# sender can not change
	nodes_g1 = [nodes_in[0]]
	nodes_g2 = []
	data_g1 = [data_in[0]]
	data_g2 = []
	size_g1 = mask -1
	size_g2 = nb - mask
	total_g1 = data_in[0]
	total_g2 = 0
	del data_in[0]
	del nodes_in[0]
	while (len(nodes_in) != 0):
		current_weight = data_in[0]
#		print "Mask: ",mask," - assigning ",nodes_in[0]
		total -= current_weight
		per_missing_node = total/(size_g1 + size_g2)
		if ((size_g1 > 0 and per_missing_node * size_g1 + total_g1 <= per_missing_node * size_g2 + total_g2) or size_g2 == 0):
			# Add data and node to g1
			nodes_g1.append(nodes_in[0])
			data_g1.append(data_in[0])
			size_g1 -= 1
			total_g1 += current_weight
		else:
			nodes_g2.append(nodes_in[0])
			data_g2.append(data_in[0])
			size_g2 -= 1
			total_g2 += current_weight
		del nodes_in[0]
		del data_in[0]
	# recursive op on groups
	(bn1,bd1) = GreadyBalance(nodes_g1,data_g1,mask>>1) 
	(bn2,bd2) = GreadyBalance(nodes_g2,data_g2,mask>>1) 
	# append outputs bigger amount of data first
	# return tuple of values
	return (bn1+bn2,bd1+bd2)

def LLFScatter(rank,world_size,msg_size):
	balance = 0
	if (rank == 0):
		balance += world_size*msg_size
	global first_step, second_step
	global rtrs_topo, nds_topo
	# if root balance and scatter data
	pos = cortex.get_compute_node_location(cortex.get_compute_node_id_from_rank(rank))
	if (len(first_step[0]) == 0):
		global rtr_to_grp
		mask = 0x1
		while (mask<<1 < len(rtr_to_grp)):
			mask <<= 1
		pos_root = cortex.get_compute_node_location(cortex.get_compute_node_id_from_rank(0))
		# Root is first, no data to send
		nds_data = []
		nds_data_tmp = {}
		s_nds = [ 0 ]
		s_data = [ 0 ]
		s_dest = [ 0 ]
		# Rest of correspondants added
		for grp_nb in rtr_to_grp.keys():
#			if (grp_nb == pos_root[0]):
#				continue
			if (nds_data_tmp.has_key(rtr_to_grp[grp_nb][0])):
				nds_data_tmp[rtr_to_grp[grp_nb][0]] += rtr_to_grp[grp_nb][2]*msg_size
			else:
				nds_data_tmp[rtr_to_grp[grp_nb][0]] = rtr_to_grp[grp_nb][2]*msg_size
			s_nds.append(rtr_to_grp[grp_nb][0])
			s_data.append(rtr_to_grp[grp_nb][2]*msg_size)
			s_dest.append(rtr_to_grp[grp_nb][1])
		second_step = (s_nds,s_data,s_dest)
		nds_data = sorted(nds_data_tmp.items(),key=lambda x: x[1])
		nds_data.reverse()
		# Adding root
		nds_data.insert(0,(0,0))
		nds = [x[0] for x in nds_data]
		data = [x[1] for x in nds_data]	
		first_step = GreadyBalance(nds,data,mask)
		# Each rep in root grp send linearly data to rep in corresponding grp
		grps = {}
		rtrs = {}
		for rk in range(0,world_size):
			rk_id = cortex.get_compute_node_id_from_rank(rk)
			rk_pos = cortex.get_compute_node_location(rk_id)
			# put as grps
			if (grps.has_key(rk_pos[0])):
				grps[rk_pos[0]][1] += msg_size
			else:
				grps[rk_pos[0]] = [rk,msg_size]
				rtrs[rk_pos[0]] = {}
			# store as router
			if (rtrs[rk_pos[0]].has_key(rk_pos[1])):
				rtrs[rk_pos[0]][rk_pos[1]][1] += msg_size
			else:
				rtrs[rk_pos[0]][rk_pos[1]] = [rk,msg_size]
			# Store all nodes
			if (nds_topo.has_key((rk_pos[0],rk_pos[1]))):
				nds_topo[(rk_pos[0],rk_pos[1])] = nds_topo[(rk_pos[0],rk_pos[1])] + [rk]
			else:
				nds_topo[(rk_pos[0],rk_pos[1])] = [rk]
		grps_keys = grps.keys()
		for k in grps_keys:
			# Sort data according to routers and pick representatives
			rtrs_all_data = sorted(rtrs[k].items(), key=lambda x: x[1][1])
			rtrs_all_data.reverse()
			rtrs_rep = [x[1][0] for x in rtrs_all_data]
			# put back root at the beginning
			root = 0
			if (rtr_to_grp.has_key(k) > 0):
				root = rtr_to_grp[k][1]		
			index = rtrs_rep.index(root)
			del rtrs_rep[index]
			rtrs_rep = [root] + rtrs_rep
			rtrs_data = [x[1][1] for x in rtrs_all_data]
			data_root = rtrs_data[index]
			del rtrs_data[index]
			rtrs_data = [data_root] + rtrs_data
			rtrs_len = len(rtrs_all_data)
			rtrs_mask = 0x1
			while (rtrs_mask<<1 < rtrs_len):
				rtrs_mask<<=1
			rtrs_topo[k] = GreadyBalance(rtrs_rep,rtrs_data,rtrs_mask)	
	# Scatter data to grp correspondants
	root_rtr_topo = first_step[0]
	if (root_rtr_topo.count(rank) > 0):
		root_rtr_data = first_step[1]
		balance += BinomialScatter(rank,root_rtr_topo,root_rtr_data,world_size,0)
	assert balance >= 0
#	print second_step
	if (second_step[2].count(rank) > 0):
		index = second_step[2].index(rank)
		data_size = second_step[1][index]
		if (data_size > 0):
			src = second_step[0][index]
			s = cortex.MPI_Status()
			cortex.MPI_Recv(rank,count=data_size,datatype=cortex.MPI_BYTE,source=src,tag=2,comm=cortex.MPI_COMM_WORLD,status=s)
			balance += data_size
	# One node can be the correspondant for multiple groupes
	if (second_step[0].count(rank) > 0):
		for i in range(0,len(second_step[0])):
			if (second_step[0][i] == rank):
				data_size = second_step[1][i]
				if (data_size > 0):
					dst = second_step[2][i]
					cortex.MPI_Isend(rank, count=data_size, datatype=cortex.MPI_BYTE, dest=dst, tag=2, comm=cortex.MPI_COMM_WORLD, request=world_size*world_size+rank*world_size+i)
					balance -= data_size
		for i in range(0,len(second_step[0])):
			if (second_step[0][i] == rank):
				data_size = second_step[1][i]
				if (data_size > 0):
					dst = second_step[2][i]
					s = cortex.MPI_Status()
					cortex.MPI_Wait(rank, request=world_size*world_size+rank*world_size+i, status=s)
	# Scatter among groupes
#	print balance
	assert balance >= 0
	rtr = rtrs_topo[pos[0]]
#	print "Third"
	if (rtr[0].count(rank) > 0):
		balance += BinomialScatter(rank,rtr[0],rtr[1],world_size,2)
	nds = nds_topo[(pos[0],pos[1])]
	# Binomial scatter within router
	msg_sizes = [msg_size]*len(nds)	
	balance += BinomialScatter(rank,nds,msg_sizes,world_size,3)	
	assert balance == msg_size	


def ComputeTopology(world_size):
	nds_per_grps = {}
	global rtr_to_grp
	pos = cortex.get_compute_node_location(cortex.get_compute_node_id_from_rank(0))
	nb_rtrs = cortex.get_number_of_routers()
	# Apply Floyd-Warshall algorithm to know the shortest path between routers
	shortest_path_rtrs = []	
	for i in range(0,nb_rtrs):
		shortest_path_rtrs.append([ nb_rtrs ] * nb_rtrs)
	root_grp = pos[0]
	rtrs_per_grps = {}	
	for i in range(0,nb_rtrs):
		rtr_loc = cortex.get_router_location(i)
		if (rtrs_per_grps.has_key(rtr_loc[0])):
			rtrs_per_grps[rtr_loc[0]].append(i)
		else:
			rtrs_per_grps[rtr_loc[0]] = [i]
		shortest_path_rtrs[i][i] = 0
		neighbors = cortex.get_router_neighbors(i)
		for j in neighbors:
			shortest_path_rtrs[i][j]=1
	# compute shortest paths in root grp
	rtr_root_grp = rtrs_per_grps[root_grp]
	for k in rtr_root_grp:
		for i in rtr_root_grp:
			for j in rtr_root_grp:
				d = shortest_path_rtrs[i][k] + shortest_path_rtrs[k][j]
				if (shortest_path_rtrs[i][j] > d):
					shortest_path_rtrs[i][j] = d
	for g in rtrs_per_grps.keys():
		if (g == root_grp):
			continue
		rtr_grp_target = rtrs_per_grps[g]
		whole = rtr_root_grp + rtr_grp_target
		for k in whole:
			for i in whole:
				for j in rtr_grp_target:
					d = shortest_path_rtrs[i][k] + shortest_path_rtrs[k][j]
					if (shortest_path_rtrs[i][j] > d):
						shortest_path_rtrs[i][j] = d
	# rtrs in pos[0] closer to other grps
	rep_rtr_root_grp = {}
	other = []
	for rk in range(1,world_size):
		rk_pos = cortex.get_compute_node_location(cortex.get_compute_node_id_from_rank(rk))
		if (rk_pos[0] == pos[0]):
			if (rep_rtr_root_grp.has_key(rk_pos[1]) == False ):
				rep_rtr_root_grp[rk_pos[1]]	= rk
		else:
			other.append(rk)
		if (nds_per_grps.has_key(rk_pos[0])):
			nds_per_grps[rk_pos[0]] += 1
		else:
			nds_per_grps[rk_pos[0]] = 1
	rep_rtr = rep_rtr_root_grp.values()
	rtr_to_grp_tmp = {}
	for rk in other:
		rk_cn = cortex.get_compute_node_id_from_rank(rk)
		rk_pos = cortex.get_compute_node_location(rk_cn)
		rk_rtr = cortex.get_router_from_compute_node(rk_cn)
		for src in rep_rtr:
			src_cn = cortex.get_compute_node_id_from_rank(src)
			src_rtr = cortex.get_router_from_compute_node(src_cn)
			d = shortest_path_rtrs[src_rtr][rk_rtr]
			if (rtr_to_grp_tmp.has_key(rk_pos[0])):
				if (rtr_to_grp_tmp[rk_pos[0]][2] > d):
					rtr_to_grp_tmp[rk_pos[0]] = ([src],[rk],d)
				elif (rtr_to_grp_tmp[rk_pos[0]][2] == d 
						and rtr_to_grp_tmp[rk_pos[0]][0].count(src) == 0):
					rtr_to_grp_tmp[rk_pos[0]][0].append(src)	
					rtr_to_grp_tmp[rk_pos[0]][1].append(rk)	
			else:
				rtr_to_grp_tmp[rk_pos[0]] = ([src],[rk],d)
	# rtr_to_grp_tmp[grp] = ([pot src],[pot dest], hops)
	# rtr_rep_grp[grp] = (rep,dest,#grps to rep)
	rtr_rep_grp = {}
	reps = {}
	# Select rep for each grp
	for k in rtr_to_grp_tmp.keys():
		mini = world_size
		val = 0
		dest = 0
		for i in range(0,len(rtr_to_grp_tmp[k][0])):
			key = rtr_to_grp_tmp[k][0][i]
			cur_d = 0
			if (reps.has_key(key)):
				cur_d = reps[key]
			if (cur_d < mini):
				mini = cur_d
				val = key
				dest = rtr_to_grp_tmp[k][1][i]
		reps[val] = cur_d + 1
		rtr_rep_grp[k] = (val,dest,mini)
	# rtr_to_grp[grp] = (rep,dest,#grps_to_rep)
	for k in rtr_rep_grp.keys():
		rtr_to_grp[k] = (rtr_rep_grp[k][0],rtr_rep_grp[k][1],nds_per_grps[k])
#	print rtr_to_grp	

def GenerateEvents(thread):
	ws = cortex.comm_world_size()
	global initialized, msg_size
	if(not initialized):
		initialize()
		ComputeTopology(ws)
	LLFScatter(thread,ws,msg_size)

