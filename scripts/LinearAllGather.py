import cortex
import random
import yaml
import os

msg_size = 0
initialized = False

def find_config():
  for f in os.listdir('.'):
    if f.endswith('.yml'):
      return f
  return None

def initialize():
  global msg_size, initialized
  filename = find_config()
  if filename == None:
    raise RuntimeError("configuration file not found")
  yaml_file = open(filename)
  yaml_data = yaml_file.read()
  params = yaml.load(yaml_data)
  params = params['codes']['parameters']
  msg_size = params['msg_size']
  initialized = True

# Linear scatter 
# rank - MPI rank of the node
# size - Size of the allocation, number of nodes participating in the communication
# msg_size - size of each message to transmit 
def AllGather(rank,size,msg_size):
	# data sent and received 
	# If the node isn't the root, wait to receive data
		# Receive one data from rank 0
	# If the node is the root send to all nodes 
	# Send all data to destinations one by one
	requests = []
	statuses = []
	for i in range(1,size):
		dst = rank + i
		if (dst >= size):
			dst -= size
		requests.append(rank*size+dst)
		statuses.append(cortex.MPI_Status())
		cortex.MPI_Isend(rank, count=msg_size, datatype=cortex.MPI_BYTE, dest=dst, tag=1234, comm=cortex.MPI_COMM_WORLD, request=rank*size+dst)
		src = rank - i
		if (src < 0):
			src += size
		requests.append(size*size+rank*size+dst)
		statuses.append(cortex.MPI_Status())
		cortex.MPI_Irecv(rank, count=msg_size, datatype=cortex.MPI_BYTE,source=src,tag=1234,comm=cortex.MPI_COMM_WORLD, request=size*size+rank*size+src)
	cortex.MPI_Waitall(rank, count=len(statuses), statuses=statuses, requests=requests)


def GenerateEvents(thread):
	ws = cortex.comm_world_size()
	global initialized, msg_size
	if(not initialized):
		initialize()
	AllGather(thread,ws,msg_size)

