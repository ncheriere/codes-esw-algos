import cortex
import random
import yaml
import os
import math

msg_size = 0
initialized = False
ring = []
target_size = 256

def find_config():
  for f in os.listdir('.'):
    if f.endswith('.yml'):
      return f
  return None

def initialize():
  global msg_size, initialized
  filename = find_config()
  if filename == None:
    raise RuntimeError("configuration file not found")
  yaml_file = open(filename)
  yaml_data = yaml_file.read()
  params = yaml.load(yaml_data)
  params = params['codes']['parameters']
  msg_size = params['msg_size']
  initialized = True

target_size = 256.0

def AllGather(rank,size,msg_size):
	global ring, target_size

	nb_msg_second_phase = int(math.ceil(target_size/msg_size)) 
	if (nb_msg_second_phase > size):
		nb_msg_second_phase = size
	print ""
	print "Target_size: ",target_size 
	print "msg_size: ",msg_size 
	print "division: ",target_size/msg_size 
	print nb_msg_second_phase," messages to receive for the second phase" 
	# data sent and received 
	balance = msg_size
	
	pos = ring.index(rank)


	step = int(math.ceil(size*1.0/nb_msg_second_phase))
	print "Step: ",step 
	if (nb_msg_second_phase > 1):
		
		for delta in range(1,nb_msg_second_phase):
			dst = (pos + delta*step) % size
			cortex.MPI_Isend(rank, count=msg_size, datatype=cortex.MPI_BYTE, dest=ring[dst], tag=1234, comm=cortex.MPI_COMM_WORLD, request=size*rank+dst)
		for delta in range(1,nb_msg_second_phase):
			src = (pos - delta*step)	
			if (src < 0):
				src += size
			cortex.MPI_Irecv(rank, count=msg_size, datatype=cortex.MPI_BYTE, source=ring[src], tag=1234, comm=cortex.MPI_COMM_WORLD, request=size*size+size*rank+src)
		for delta in range(1,nb_msg_second_phase):
			dst = (pos + delta*step) % size
			s = cortex.MPI_Status()
			cortex.MPI_Wait(rank,request=size*rank+dst,status=s)
		for delta in range(1,nb_msg_second_phase):
			src = (pos - delta*step)	
			if (src < 0):
				src += size
			s = cortex.MPI_Status()
			cortex.MPI_Wait(rank,request=size*size+size*rank+src,status=s)
			balance += msg_size
#	
	assert balance == msg_size * nb_msg_second_phase
	second_msg_size = msg_size*nb_msg_second_phase
	for delta in range(1,step):
		dst = (pos + delta) % size
		cortex.MPI_Isend(rank, count=second_msg_size, datatype=cortex.MPI_BYTE, dest=ring[dst], tag=1234, comm=cortex.MPI_COMM_WORLD, request=2*size*size+size*rank+dst)
	for delta in range(1,step):
		src = (pos - delta)	
		if (src < 0):
			src += size
		cortex.MPI_Irecv(rank, count=second_msg_size, datatype=cortex.MPI_BYTE, source=ring[src], tag=1234, comm=cortex.MPI_COMM_WORLD, request=3*size*size+size*rank+src)
		
	for delta in range(1,step):
		dst = (pos + delta) % size
		s = cortex.MPI_Status()
		cortex.MPI_Wait(rank,request=2*size*size+size*rank+dst,status=s)
	for delta in range(1,step):
		src = (pos - delta)	
		if (src < 0):
			src += size
		s = cortex.MPI_Status()
		cortex.MPI_Wait(rank,request=3*size*size+size*rank+src,status=s)
		balance += second_msg_size
	print "Extra balance: ",(balance-msg_size*size) 
	assert balance >= msg_size * size




# Sort nodes according to their compute node id
def CreateRing(world_size):
	global ring
	ranks = {}
	for i in range(0,world_size):
		ranks[i] = cortex.get_compute_node_id_from_rank(i)
	sorted_ranks = sorted(ranks.items(),key=lambda x: x[1])
	ring = [x[0] for x in sorted_ranks]

def GenerateEvents(thread):
	ws = cortex.comm_world_size()
	global initialized, num_iter, msg_size
	if(not initialized):
		initialize()
		CreateRing(ws)
	AllGather(thread,ws,msg_size)
