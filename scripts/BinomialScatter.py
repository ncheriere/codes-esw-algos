import cortex
import random
import yaml
import os

msg_size = 0
initialized = False

def find_config():
  for f in os.listdir('.'):
    if f.endswith('.yml'):
      return f
  return None

def initialize():
  global msg_size, initialized
  filename = find_config()
  if filename == None:
    raise RuntimeError("configuration file not found")
  yaml_file = open(filename)
  yaml_data = yaml_file.read()
  params = yaml.load(yaml_data)
  params = params['codes']['parameters']
  msg_size = params['msg_size']
  initialized = True

# Scatter data with binomial tree
# rank - id of the current node
# size - size of the world, number of nodes participating in the communication
# msg_size - size of each data to scatter 
def Scatter(rank,size,msg_size):
	balance = 0
	if (rank == 0):
		balance += size*msg_size
	mask = 0x1;
	# receive phase
	# Each node (except root) will receive data once
	# The amount of data it receives and from who depends on the mask
	while(mask < size):
		if(rank & mask):
			src = rank - mask
			assert src >= 0 
			s = cortex.MPI_Status()
			data = min(mask,size-rank)*msg_size
			cortex.MPI_Recv(rank,count=data,datatype=cortex.MPI_BYTE,source=src,tag=1234,comm=cortex.MPI_COMM_WORLD,status=s)
			balance += data
			break
		mask <<= 1
	# send phase
	# Once data is received, the node has to send it back to following nodes
	# (Except for its own data) 
	mask >>= 1;
	while(mask > 0):
		if(rank + mask < size):
			dst = rank + mask
			assert dst < size
			data = min(mask,size-dst)*msg_size
			cortex.MPI_Send(rank,count=data,datatype=cortex.MPI_BYTE,dest=dst,tag=1234,comm=cortex.MPI_COMM_WORLD)
			balance -= data
		mask >>= 1;
	assert balance == msg_size


def GenerateEvents(thread):
  ws = cortex.comm_world_size()
  global initialized, msg_size
  if(not initialized):
    initialize()
  Scatter(thread,ws,msg_size)

