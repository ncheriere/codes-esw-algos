#!/bin/bash

MASTER=$( head $OAR_FILE_NODES | uniq )
echo "Master: "$MASTER

cat $OAR_FILE_NODES | uniq > Dragonfly/CODES-ESW/codes-esw/hosts.txt
kadeploy3 -a Dragonfly/Dragonfly-img-0.1.env -f $OAR_FILE_NODES -k

NB_NODES=$( wc -l $OAR_FILE_NODES | grep -o "[0-9]*" | head -1 ) 
echo $NB_NODES" cores available."

NB_INSTANCES=$(( $NB_NODES / 16 * 10 ))
echo $NB_INSTANCES" instances to launch"

ssh $MASTER "./launch.sh "$NB_INSTANCES" config/scatter-bigalloc-adaptive.yml"
