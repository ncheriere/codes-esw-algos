#!/bin/bash

source /home/ncheriere/Dragonfly/CODES-ESW/codes-esw/profile.sh

cd Dragonfly/CODES-ESW/codes-esw

./clean.sh
./build_esw.sh
echo "Launching CODES with "$1" with conf file "$2
./run.sh $1 hosts.txt config/allgather.yml
mv build build-allgather
./archive.sh build-allgather

echo "done"
